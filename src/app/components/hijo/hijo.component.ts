import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'componente-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  public title: string;

  // esta propiedad, nos va a llegar desde fuera
  @Input() propiedad_uno: string;
  @Input() propiedad_dos: string;

  @Output() desde_el_hijo = new EventEmitter();

  constructor() {
    this.title = 'Componente hijo';
   }

  ngOnInit() {
    console.log(this.propiedad_uno);
    console.log(this.propiedad_dos);
  }

  enviar(event) {
    this.desde_el_hijo.emit({nombre: 'Reinhold valdivia Outpout'});
  }

}
