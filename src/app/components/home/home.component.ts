import { Component, OnInit } from '@angular/core';
import { RopaService } from '../../services/ropa.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [RopaService]
})
export class HomeComponent implements OnInit {

  public title = 'Página principal HOME';
  public listadoRopa: Array<string>;
  public prendaGuardar:string;

  public fecha;

  constructor(
    private _ropaService: RopaService
  ) {
    this.fecha = new Date(2017, 4, 15);
  }

  ngOnInit() {
    this.listadoRopa = this._ropaService.getRopa();
    console.log(this.listadoRopa);

  }

  guardarPrenda() {
    this._ropaService.addRopa(this.prendaGuardar);
    this.prendaGuardar = null;
  }

  eliminarPrenda(indice: number) {
    this._ropaService.deleteRopa(indice);
  }

}
