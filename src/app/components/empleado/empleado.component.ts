import { Component, OnInit } from '@angular/core';
import { Empleado } from './empleado';

@Component({
  selector: 'empleado-tag',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  public titulo = 'componente de empleado';
  public empleado: Empleado;
  public trabajadores: Array<Empleado>;
  public trabajadorExterno: boolean;
  public color: string;
  public colorSeleccionado: string;

  constructor() {
    this.empleado = new Empleado('David', 45, 'administrativo', true);
    this.trabajadores = [
      new Empleado ('Juan', 25, 'programador', true),
      new Empleado ('Pedro', 66, 'informatico', false),
      new Empleado ('Menual', 25, 'camarero', true)
    ];
    this.trabajadorExterno = false;
    this.color = 'green';
    this.colorSeleccionado = '#CCC';
   }

  ngOnInit() {
    console.log(this.empleado);
    console.log(this.trabajadores);
  }

  cambiarExterno(valor) {
    this.trabajadorExterno = valor;
  }

}
