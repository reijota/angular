import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'plantillas',
  templateUrl: './plantillas.component.html',
  styleUrls: ['./plantillas.component.css']
})
export class PlantillasComponent implements OnInit {

  public titulo;
  public administrador;

  public datoExterno = 'Reinhold valdivia';
  public identity = {
    id: 1,
    web: 'reinholdvaldivia.com',
    tematica: 'Desarrollo web'
  };

  public datos_del_hijo;

  constructor() {
    this.titulo = 'Plantillas ngTemplate en Angular';
    this.administrador = true;
  }

  ngOnInit() {
  }

  cambiar(value) {
    this.administrador = value;
  }

  recibirDatos(event) {
    console.log(event.nombre);
    this.datos_del_hijo = event;
  }
}
