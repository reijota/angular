import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// importar componentes
import { EmpleadoComponent } from './components/empleado/empleado.component';
import { FrutaComponent } from './components/fruta/fruta.component';
import { HomeComponent } from './components/home/home.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { CochesComponent } from './components/coches/coches.component';
import { PlantillasComponent } from './components/plantillas/plantillas.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'pagina-principal', component: HomeComponent},
  {path: 'empleado', component: EmpleadoComponent},
  {path: 'fruta', component: FrutaComponent },
  {path: 'contacto', component: ContactoComponent },
  {path: 'contacto/:page', component: ContactoComponent },
  {path: 'coches', component: CochesComponent },
  {path: 'plantillas', component: PlantillasComponent },
  {path: '**', component: HomeComponent}
];

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
