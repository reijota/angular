import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RopaService {

  public nombrePrenda = 'Pantalones vaqueros';
  public coleccionRopa = ['pantalones blancos', 'camiseta roja'];

  constructor() { }

  prueba(nombrePrenda) {
    return nombrePrenda;
  }

  addRopa(nombrePrenda: string) {
    this.coleccionRopa.push(nombrePrenda);
    return this.getRopa;
  }

  deleteRopa(indice: number) {
    this.coleccionRopa.splice(indice, 1);
    return this.getRopa;
  }

  getRopa() {
    return this.coleccionRopa;
  }

}
