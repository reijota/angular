import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  public url: string;
  public errorHandl;

  constructor(private _http: HttpClient) {
    this.url = 'https://jsonplaceholder.typicode.com/posts';
   }

  getPrueba() {
    return 'Hola desde el servicio';
  }

  getArticulos() {
    return this._http.get(this.url)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
}
